# Spike Report

## Spike 1 - Development Environment

### Introduction

The purpose of this spike is to teach us how to set up a basic Unreal Engine work environment using git.

### Goals

The goal of this spike was to create a git repository, which contains:

* A proper gitignore file for Unreal Engine as the first commit
* The Unreal Engine First Person Shooter C++ project, without Starter Materials
* A spike report used as the README.md for the repository

### Personnel

* Primary - Mitchell Bailey
* Secondary - N/A

### Technologies, Tools, and Resources used

* [Unreal Engine .gitignore File](https://github.com/github/gitignore/blob/master/UnrealEngine.gitignore)

### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Created bitbucket repository
1. Generated .gitignore through bitbucket and replaced contents with the contents of the Unreal Engine .gitignore file
1. Added README.md to respository
1. Created First Person Shooter project without starter content
1. Moved contents of Unreal project directory to root git directory to make the .gitignore function correctly

### What we found out

1. How to create projects in Unreal
1. How to use git/SourceTree
1. How .gitignore files operate

### [Optional] Open Issues/Risks



### [Optional] Recommendations

